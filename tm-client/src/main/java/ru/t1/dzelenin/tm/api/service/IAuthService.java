package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.UserDTO;
import ru.t1.dzelenin.tm.enumerated.Role;

public interface IAuthService {

    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @Nullable
    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    UserDTO getUser();

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);


}



