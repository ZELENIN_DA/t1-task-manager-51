package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.api.repository.IRepository;
import ru.t1.dzelenin.tm.dto.model.AbstractModelDTO;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {
}


