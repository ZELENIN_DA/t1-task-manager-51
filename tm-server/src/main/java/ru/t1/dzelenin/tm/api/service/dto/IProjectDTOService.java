package ru.t1.dzelenin.tm.api.service.dto;

import ru.t1.dzelenin.tm.dto.model.ProjectDTO;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {
}