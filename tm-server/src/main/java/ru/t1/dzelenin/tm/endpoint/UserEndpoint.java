package ru.t1.dzelenin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.endpoint.IUserEndpoint;
import ru.t1.dzelenin.tm.api.service.IServiceLocator;
import ru.t1.dzelenin.tm.api.service.dto.IUserDTOService;
import ru.t1.dzelenin.tm.dto.model.SessionDTO;
import ru.t1.dzelenin.tm.dto.model.UserDTO;
import ru.t1.dzelenin.tm.dto.request.user.*;
import ru.t1.dzelenin.tm.dto.response.user.*;
import ru.t1.dzelenin.tm.enumerated.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.dzelenin.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserDTOService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable UserDTO user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable UserDTO user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    public UserProfileResponse viewProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        if (userId == null) return new UserProfileResponse();
        @Nullable UserDTO user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        if (userId == null) return new UserChangePasswordResponse();
        @Nullable final String password = request.getPassword();
        if (password == null) return new UserChangePasswordResponse();
        @Nullable UserDTO user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

}