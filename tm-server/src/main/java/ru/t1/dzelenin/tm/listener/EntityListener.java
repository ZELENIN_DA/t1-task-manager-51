package ru.t1.dzelenin.tm.listener;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.component.JmsComponent;
import ru.t1.dzelenin.tm.enumerated.EntityOperationType;

import static ru.t1.dzelenin.tm.enumerated.EntityOperationType.*;

@NoArgsConstructor
public class EntityListener implements PostUpdateEventListener, PostDeleteEventListener, PostInsertEventListener,
        PreUpdateEventListener, PreDeleteEventListener, PreInsertEventListener, PostLoadEventListener {

    @NotNull
    private final JmsComponent jmsComponent = new JmsComponent();

    @SneakyThrows
    private void sendMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        jmsComponent.sendMessage(entity, operationType.toString());
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        sendMessage(postDeleteEvent.getEntity(), POST_REMOVE);
    }

    @Override
    public void onPostInsert(PostInsertEvent postInsertEvent) {
        sendMessage(postInsertEvent.getEntity(), POST_PERSIST);
    }

    @Override
    public void onPostUpdate(PostUpdateEvent postUpdateEvent) {
        sendMessage(postUpdateEvent.getEntity(), POST_UPDATE);
    }

    @Override
    public void onPostLoad(PostLoadEvent postLoadEvent) {
        sendMessage(postLoadEvent.getEntity(), POST_LOAD);
    }

    @Override
    public boolean onPreDelete(PreDeleteEvent preDeleteEvent) {
        sendMessage(preDeleteEvent.getEntity(), PRE_REMOVE);
        return false;
    }

    @Override
    public boolean onPreInsert(PreInsertEvent preInsertEvent) {
        sendMessage(preInsertEvent.getEntity(), PRE_PERSIST);
        return false;
    }

    @Override
    public boolean onPreUpdate(PreUpdateEvent preUpdateEvent) {
        sendMessage(preUpdateEvent.getEntity(), PRE_UPDATE);
        return false;
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

}
