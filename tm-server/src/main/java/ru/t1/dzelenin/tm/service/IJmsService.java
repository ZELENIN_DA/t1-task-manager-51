package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.LogDto;

public interface IJmsService {

    void send(@NotNull LogDto entity);

    LogDto createMessage(@NotNull Object object, @NotNull String type);

}
