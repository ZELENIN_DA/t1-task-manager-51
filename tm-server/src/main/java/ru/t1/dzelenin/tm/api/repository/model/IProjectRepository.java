package ru.t1.dzelenin.tm.api.repository.model;

import ru.t1.dzelenin.tm.dto.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}