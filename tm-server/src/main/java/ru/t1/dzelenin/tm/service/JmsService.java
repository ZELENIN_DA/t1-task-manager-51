package ru.t1.dzelenin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.LogDto;

import javax.jms.*;
import java.util.Date;

public class JmsService implements IJmsService {

    @NotNull
    private static final String TOPIC_NAME = "JCG_TOPIC";

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @Override
    @SneakyThrows
    public void send(@NotNull final LogDto entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final javax.jms.Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(TOPIC_NAME);
        final MessageProducer producer = session.createProducer(destination);
        final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

    @Override
    @SneakyThrows
    public LogDto createMessage(@NotNull final Object object, @NotNull final String type) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        @NotNull final String className = object.getClass().getSimpleName();
        @NotNull final LogDto message = new LogDto(className, new Date().toString(), type, json);
        return message;
    }

}